# Program steps

1. Get trade sequences
2. Calculate results for all sequences
3. Get the most profitable trade sequence
4. Execute trade sequence 
5. If trade sequence is not successful convert to source currency

