
const generatePairId = (baseCurrency, quoteCurrency) => {
    return `${baseCurrency}-${quoteCurrency}`;
};

export default generatePairId;