const fs = require('fs');

export default class LoggerService {

	baseLogPath: string = null;
	balanceFile: string = 'balance';
	whileBalanceFile: string = 'while-balance';
	errorFile: string = 'errors';
	logFile: string = 'log';

	constructor(baseLogPath: string) {
		this.baseLogPath = baseLogPath;

		this.createLogFolder();
	}

	getDate() {
		return new Date().toISOString().slice(0,10);
	}
	getTimestamp() {
		return new Date().toISOString();
	}

	createLogFolder() {
		if (!fs.existsSync(this.baseLogPath)){
			fs.mkdirSync(this.baseLogPath);
		}
	}

	logError(error: string) {
		fs.appendFile(
			`${ this.baseLogPath }/${ this.getDate() }_${ this.errorFile }.log`,
			this.getTimestamp() + ' ' + error + '\n', () => {});
	}

	logBalance(balance: string) {
		fs.appendFile(
			`${ this.baseLogPath }/${ this.getDate() }_${ this.balanceFile }.log`,
			this.getTimestamp() + ' ' + balance + '\n', () => {});
	}
	logWholeBalance(balance: string) {
		fs.appendFile(
			`${ this.baseLogPath }/${ this.getDate() }_${ this.whileBalanceFile }.log`,
			this.getTimestamp() + ' ' + balance + '\n', () => {});
	}

	log(log) {
		fs.appendFile(
			`${ this.baseLogPath }/${ this.getDate() }_${ this.logFile }.log`,
			this.getTimestamp() + ' ' + log + '\n', () => {});
	}
}
