export interface OrderBookEntry {
  price: number;
  amount: number;
}