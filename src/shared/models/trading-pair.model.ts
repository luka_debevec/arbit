import {OrderBookEntry} from './order-book-entry.model';

export interface TradingPair {
  id: string;
  baseCurrency: string; // buying
  quoteCurrency: string; // selling
  ask: OrderBookEntry;
  bid: OrderBookEntry;
  baseCurrencyPrecision: number;
  quoteCurrencyPrecision: number;
  minAmount: number;
  minTotal: number;
}