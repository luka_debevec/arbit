import {TradingPair} from './trading-pair.model';

export interface TradingPairs {
  [key: string]: TradingPair
};

export const exampleTradingPairs: TradingPairs = {
  'A-B': {
    id: 'A-B',
    baseCurrency: 'A',
    quoteCurrency: 'B',
    minAmount: 1,
    minTotal: 1,
    ask: {price: 10, amount: 5},
    bid: {price: 10, amount: 5},
    baseCurrencyPrecision: 2,
    quoteCurrencyPrecision: 1,
  },
  'A-C': {
    id: 'A-C',
    baseCurrency: 'A',
    quoteCurrency: 'C',
    minAmount: 1,
    minTotal: 1,
    ask: {price: 5, amount: 3},
    bid: {price: 5, amount: 3},
    baseCurrencyPrecision: 4,
    quoteCurrencyPrecision: 5,
  },
  'A-D': {
    id: 'A-D',
    baseCurrency: 'A',
    quoteCurrency: 'D',
    minAmount: 1,
    minTotal: 2,
    ask: {price: 2, amount: 2},
    bid: {price: 1, amount: 0.06},
    baseCurrencyPrecision: 2,
    quoteCurrencyPrecision: 3,
  },
  'B-C': {
    id: 'B-C',
    baseCurrency: 'B',
    quoteCurrency: 'C',
    minAmount: 1,
    minTotal: 1,
    ask: {price: 3, amount: 3},
    bid: {price: 3, amount: 3},
    baseCurrencyPrecision: 2,
    quoteCurrencyPrecision: 3,
  },
  'C-D': {
    id: 'C-D',
    baseCurrency: 'C',
    quoteCurrency: 'D',
    minAmount: 1,
    minTotal: 2,
    ask: {price: 2, amount: 2},
    bid: {price: 1, amount: 0.06},
    baseCurrencyPrecision: 2,
    quoteCurrencyPrecision: 3,
  },
};