// import {exampleTradingPairs} from '../../shared/models/trading-pairs.model';
import {AlgorithmParams} from '../models/algorithm-params.model';
import {ArbitHelper} from './arbit.helper';
import {TradingPairs} from "../../shared/models/trading-pairs.model";
import OrderType from "../models/order-type.enum";

const exampleTradingPairs: TradingPairs = {
  'A-B': {
    id: 'A-B',
    baseCurrency: 'A',
    quoteCurrency: 'B',
    minAmount: 0.1,
    minTotal: 0.1,
    ask: {price: 10, amount: 5},
    bid: {price: 10, amount: 5},
    baseCurrencyPrecision: 2,
    quoteCurrencyPrecision: 1,
  },
  'B-C': {
    id: 'B-C',
    baseCurrency: 'B',
    quoteCurrency: 'C',
    minAmount: 0.01,
    minTotal: 0.01,
    ask: {price: 3, amount: 3},
    bid: {price: 3, amount: 3},
    baseCurrencyPrecision: 2,
    quoteCurrencyPrecision: 3,
  },
  'A-C': {
    id: 'A-C',
    baseCurrency: 'A',
    quoteCurrency: 'C',
    minAmount: 0.01,
    minTotal: 0.01,
    ask: {price: 5.2, amount: 3},
    bid: {price: 5.1, amount: 3},
    baseCurrencyPrecision: 4,
    quoteCurrencyPrecision: 5,
  },
};

fdescribe('ArbitHelper', () => {
  describe('getOpurtunities', () => {
    it('should return the most profitable trade sequence', () => {
      const algorithmParams: AlgorithmParams = {
        sourceCurrency: 'A',
        targetCurrency: 'A',
        maxDepth: 3,
      };
      const exchangeParams = {
        fee: 0.1
      };
      const bestPath = ArbitHelper.getProfitableTradeSequence(exampleTradingPairs, algorithmParams, exchangeParams, 5,{});
      const expectedSequenceResult = {
        orders: [
          {
            pair: 'A-B',
            amount: 0.3,
            price: 10,
            type: OrderType.Sell,
            baseCurrencyPrecision: 2,
            quoteCurrencyPrecision: 1
          },
          {
            pair: 'B-C',
            amount: 2.7,
            price: 3,
            type: OrderType.Sell,
            baseCurrencyPrecision: 2,
            quoteCurrencyPrecision: 3
          },
          {
            pair: 'A-C',
            amount: 1.4019,
            price: 5.2,
            type: OrderType.Buy,
            baseCurrencyPrecision: 4,
            quoteCurrencyPrecision: 5
          }
        ],
        roundingLoss: 0.000792575155939161,
        inputAmount: 0.3,
        outputAmount: 1.2617
      };


      expect(bestPath).toEqual(expectedSequenceResult);
    });

    it('should return null if no profitable trade sequence was found', () => {
      const algorithmParams: AlgorithmParams = {
        sourceCurrency: 'A',
        targetCurrency: 'A',
        maxDepth: 3,
      };
      const exchangeParams = {
        fee: 0.5
      };
      const bestPath = ArbitHelper.getProfitableTradeSequence(exampleTradingPairs, algorithmParams, exchangeParams, 1, {});


      expect(bestPath).toEqual(null);
    });
  });
});
