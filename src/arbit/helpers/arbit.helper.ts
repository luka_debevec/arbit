import Big from 'big.js';
import {ExchangeParameters} from '../../shared/models/exchange-parameters.model';
import {TradingPair} from '../../shared/models/trading-pair.model';
import {TradingPairs} from '../../shared/models/trading-pairs.model';
import buildTradeMap from '../functions/build-trade-map';
import {AlgorithmParams} from '../models/algorithm-params.model';
import getTradeSequences from "../functions/get-trade-sequences.function";
import {calculateTradeResult} from "../functions/calculate-trade-result.function";
import Order from "../models/order.model";
import {Balance} from "../../shared/models/balance.model";


export interface TradeSequence {
  orders: Order[],
  inputAmount: number,
  outputAmount: number
}


export class ArbitHelper {
  static getPossibleTradePaths(
    tradingPairs: TradingPairs,
    algorithmParams: AlgorithmParams,
  ): TradingPair[][] {
    const {sourceCurrency, targetCurrency, maxDepth} = algorithmParams;
    const tradeMap = buildTradeMap(tradingPairs);
    const pathSettings = {
      algorithmParams,
      tradingPairs,
      tradeMap,
      currentCurrency: sourceCurrency,
      currentPairs: []
    };

    const tradeSequences = getTradeSequences(algorithmParams, pathSettings);

    return tradeSequences;
  }

  static getProfitableTradeSequence(
    tradingPairs: TradingPairs,
    algorithmParams: AlgorithmParams,
    exchangeParams: ExchangeParameters,
    sourceCurrencyBalance: number,
    balance: Balance,
    threshold: number = 1): TradeSequence | null {

    const possiblePaths = ArbitHelper.getPossibleTradePaths(tradingPairs, algorithmParams);
    const sourceCurrencyInfo = {
      balance: sourceCurrencyBalance,
      id: algorithmParams.sourceCurrency
    };
    let bestTrade = null;
    let currentMax = -1;

    for (const possiblePath of possiblePaths) {
      const tradeResult = calculateTradeResult(possiblePath, sourceCurrencyInfo, balance, exchangeParams.fee);

      if (tradeResult) {
        const { orders, inputAmount, outputAmount } = tradeResult;
        const tradeResultPercentage = +(new Big(outputAmount).div(inputAmount)).toFixed();

        if (tradeResultPercentage > currentMax) {
          bestTrade = tradeResult;
          currentMax = tradeResultPercentage;
        }
      }
    }

    if (currentMax > threshold) {
      return bestTrade;
    }

    return null;
  }
}
