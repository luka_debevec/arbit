import {TradingPairs} from '../../shared/models/trading-pairs.model';

const buildTradeMap = (tradingPairs: TradingPairs) => {
  const tradingPairIds = Object.keys(tradingPairs);
  const sellMap = {};
  const buyMap = {};

  for (let tradingPairId of tradingPairIds) {
    const [baseCurrency, quoteCurrency] = tradingPairId.split('-');

    if (!sellMap[baseCurrency]) {
      sellMap[baseCurrency] = [quoteCurrency];
    }
    else {
      sellMap[baseCurrency].push(quoteCurrency);
    }

    if (!buyMap[quoteCurrency]) {
      buyMap[quoteCurrency] = [baseCurrency];
    }
    else {
      buyMap[quoteCurrency].push(baseCurrency);
    }
  }

  return {sellMap, buyMap};
};

export default buildTradeMap;