import {TradingPair} from '../../shared/models/trading-pair.model';
import Big from 'big.js';
import OrderType from "../models/order-type.enum";
import Order from "../models/order.model";
import RoundingType from "../models/rounding-type.enum";

Big.RM = 0;


const getOrderTypeFromPairId = (sourceCurrencyId: string, pairId: string) => {
	const [baseCurrency, quoteCurrency] = pairId.split('-');

	return sourceCurrencyId === baseCurrency ?
		OrderType.Sell :
		OrderType.Buy;
};

const createSellOrder = (balance, tradePair: TradingPair): Order => {
	const {bid, baseCurrencyPrecision, quoteCurrencyPrecision} = tradePair;
	const {price, amount} = bid;
	const orderAmount = +(new Big(Math.min(balance, amount)).toFixed(baseCurrencyPrecision));

	if (orderAmount < tradePair.minAmount) {
		return null;
	}

	return {
		pair: tradePair.id,
		amount: orderAmount,
		price,
		type: OrderType.Sell,
		baseCurrencyPrecision,
		quoteCurrencyPrecision,
	}
};

const createBuyOrder = (balance, tradePair: TradingPair): Order => {
	const {ask, baseCurrencyPrecision, quoteCurrencyPrecision} = tradePair;
	const {price, amount} = ask;
	const calculatedAmount = new Big(balance).div(price).toFixed(tradePair.baseCurrencyPrecision);
	const orderAmount = Math.min(calculatedAmount, amount);

	if (orderAmount < tradePair.minAmount) {
		return null;
	}

	return {
		pair: tradePair.id,
		amount: orderAmount,
		price,
		type: OrderType.Buy,
		baseCurrencyPrecision,
		quoteCurrencyPrecision,
	}
};

const createOrder = (sourceCurrency: { id: string, balance: number }, tradePair: TradingPair) => {
	const orderType = getOrderTypeFromPairId(sourceCurrency.id, tradePair.id);

	if (orderType === OrderType.Sell) {
		return createSellOrder(sourceCurrency.balance, tradePair);
	} else {
		return createBuyOrder(sourceCurrency.balance, tradePair);
	}
};

export const getTargetCurrency = (sourceCurrencyId: string, pairId: string) => {
	const [baseCurrency, quoteCurrency] = pairId.split('-');

	return baseCurrency === sourceCurrencyId
		? quoteCurrency
		: baseCurrency;
};

export const calculateOrderResult = (order: Order, feePercentage, pair: TradingPair) => {
	const {amount, type, price} = order;
	let value = (new Big(amount));
	let precision = pair.baseCurrencyPrecision;

	if (type === OrderType.Sell) {
		value = value.times(price);
		precision = pair.quoteCurrencyPrecision;
	}

	let valueAfterFees = value.times(1 - feePercentage);
	let valueAfterRounding = valueAfterFees.toFixed(precision);
	let roundingLoss = valueAfterFees.minus(valueAfterRounding);

	return {
		value: +valueAfterRounding,
		lossPercentage: +roundingLoss.div(valueAfterFees).times(100).toFixed(),
	};
};

const printOrderToConsole = (order: Order) => {
	const [baseCurrency, quoteCurrency] = order.pair.split('-');

	if (order.type === OrderType.Buy) {
		console.log('BUY: ' + baseCurrency);
		console.log(order);
		console.log('result: ', order.amount + ' ' + baseCurrency)
	} else {
		console.log('SELL: ' + baseCurrency + ' amount ' + order.amount);
		console.log('result: ', order.amount * order.price + ' ' + quoteCurrency)
	}
};

const calculateOrdersSequence = (sourceCurrency, orders: Order[], tradePairs: TradingPair[], fee) => {
	let currentCurrency = {
		...sourceCurrency
	};

	const firstOrder = orders[0];
	const {value, lossPercentage} = calculateOrderResult(firstOrder, fee, tradePairs[0]);
	let roundingLoss = lossPercentage;

	currentCurrency.id = getTargetCurrency(currentCurrency.id, firstOrder.pair);
	currentCurrency.balance = value;
	
	const inputAmount = firstOrder.type === OrderType.Sell ?
		firstOrder.amount :
		+(new Big(firstOrder.amount)).times(firstOrder.price).toFixed();

	if (firstOrder.amount < tradePairs[0].minAmount) {
		return null;
	}



	for (let i = 1; i < orders.length; i++ ) {
		const tradePair = tradePairs[i];
		const order = createOrder(currentCurrency, tradePair);

		if (!order) {
			return null;
		}

		const {value, lossPercentage} = calculateOrderResult(order, fee, tradePair);
		if (value < tradePair.minTotal) {
			return null;
		}

		roundingLoss +=lossPercentage;

		orders[i] = order;
		currentCurrency.id = getTargetCurrency(currentCurrency.id, order.pair);
		currentCurrency.balance = value;
	}

	return {
		orders,
		roundingLoss,
		inputAmount,
		outputAmount: currentCurrency.balance,
	};
};
function getPrecision(minIncrement: string) {
	const [, decimal] = minIncrement.split('.');

	return decimal ? decimal.length : 0;
}
export const getMinIncrement = (decimal: number) => {
	const divideWith = new Big(10).pow(decimal);

	return + (new Big(1).div(divideWith).toFixed());
};

export const calculateTradeResult = (tradePairs: TradingPair[],
																		 sourceCurrency: {
																			 id: string;
																			 balance: number;
																		 },
																		 balance,
																		 fee: number = 0.2) => {
	let orders: Order[] = [];
	let currentCurrency = {
		...sourceCurrency,
	};

	for (let tradePair of tradePairs) {
		const order = createOrder(currentCurrency, tradePair);


		if (!order) {
			return null;
		}

		const {value, lossPercentage} = calculateOrderResult(order, fee, tradePair);

		orders.push(order);

		currentCurrency.id = getTargetCurrency(currentCurrency.id, order.pair);
		const currentCurrencyBalance = balance[currentCurrency.id];

		if (!currentCurrencyBalance) {
			return null;
		}
		const precision = order.type === OrderType.Buy ?
				order.baseCurrencyPrecision :
				order.quoteCurrencyPrecision;
		const adjustedBalance = new Big(currentCurrencyBalance)
			.minus(new Big(getMinIncrement(precision)).times(2))
			.toFixed();
		currentCurrency.balance = Math.min(+value, +adjustedBalance);
	}

	orders = adjustOrderAmounts(orders, fee);

	if (!orders) {
		return null;
	}

	const sequenceResult  = calculateOrdersSequence(sourceCurrency, orders, tradePairs, fee);

	if (!sequenceResult) {
		return null;
	}

	let currentBestOption = { ...sequenceResult };

	for (let i = 1; i < 20; i++) {
		const tempOrders = orders.map((order) => ({ ...order }));

		tempOrders[0].amount = +(new Big(tempOrders[0].amount))
			.minus(new Big(getMinIncrement(tempOrders[0].baseCurrencyPrecision)).times(i))
			.toFixed();

		if (tempOrders[0].amount < tradePairs[0].minAmount) {
			break;
		}

		const tempSequence  = calculateOrdersSequence(sourceCurrency, tempOrders, tradePairs, fee);

		if (!tempSequence) {
			break;
		}

		if (currentBestOption.roundingLoss > tempSequence.roundingLoss) {
			currentBestOption = tempSequence;
		}
	}

	return currentBestOption;
};

export function adjustOrderAmounts(orders, feePercentage) {
	if (orders.length < 2) {
		return orders;
	}

	for (let i = orders.length - 1; i > 0; i--) {
		const order: Order = orders[i];
		const prevOrder: Order = orders[i - 1];
		const prevOrderTotal = order.type === OrderType.Buy ?
			(new Big(order.price)).times(order.amount).div(1 - feePercentage) :
			(new Big(order.amount)).div(1 - feePercentage);

		if (prevOrder.type === OrderType.Sell) {
			const newAmount = prevOrderTotal
				.div(prevOrder.price)
				.round(prevOrder.baseCurrencyPrecision, RoundingType.Up)
				.toFixed();

			if (newAmount > prevOrder.amount) {
				return null;
			}

			prevOrder.amount = +newAmount;
		} else {
			const newAmount = prevOrderTotal
				.round(prevOrder.baseCurrencyPrecision, RoundingType.Up)
				.toFixed();

			if (newAmount > prevOrder.amount) {
				return null;
			}

			prevOrder.amount = +newAmount;
		}

	}
	return orders;
}
