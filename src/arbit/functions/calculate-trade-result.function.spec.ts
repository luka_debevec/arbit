import {TradingPairs} from '../../shared/models/trading-pairs.model';
import {calculateTradeResult} from './calculate-trade-result.function';
import OrderType from "../models/order-type.enum";

const exampleTradingPairs: TradingPairs = {
  'A-B': {
    id: 'A-B',
    baseCurrency: 'A',
    quoteCurrency: 'B',
    minAmount: 0.1,
    minTotal: 0.1,
    ask: {price: 10, amount: 5},
    bid: {price: 10, amount: 5},
    baseCurrencyPrecision: 2,
    quoteCurrencyPrecision: 1,
  },
  'B-C': {
    id: 'B-C',
    baseCurrency: 'B',
    quoteCurrency: 'C',
    minAmount: 0.01,
    minTotal: 0.01,
    ask: {price: 3, amount: 3},
    bid: {price: 3, amount: 3},
    baseCurrencyPrecision: 2,
    quoteCurrencyPrecision: 3,
  },
  'A-C': {
    id: 'A-C',
    baseCurrency: 'A',
    quoteCurrency: 'C',
    minAmount: 0.01,
    minTotal: 0.01,
    ask: {price: 5.2, amount: 3},
    bid: {price: 5.1, amount: 3},
    baseCurrencyPrecision: 4,
    quoteCurrencyPrecision: 5,
  },
};

fdescribe('calculateTradeResult', () => {
  it('should return list of orders and starting and end amount', () => {
    const tradeResults = calculateTradeResult(
      [exampleTradingPairs['A-B'], exampleTradingPairs['B-C'], exampleTradingPairs['A-C']],
      {
        id: 'A',
        balance: 5,
      },
      0.1
    );
    const expectedSequenceResult = {
      orders: [
        {
          pair: 'A-B',
          amount: 0.3,
          price: 10,
          type: OrderType.Sell,
          baseCurrencyPrecision: 2,
          quoteCurrencyPrecision: 1
        },
        {
          pair: 'B-C',
          amount: 2.7,
          price: 3,
          type: OrderType.Sell,
          baseCurrencyPrecision: 2,
          quoteCurrencyPrecision: 3
        },
        {
          pair: 'A-C',
          amount: 1.4019,
          price: 5.2,
          type: OrderType.Buy,
          baseCurrencyPrecision: 4,
          quoteCurrencyPrecision: 5
        }
      ],
      roundingLoss: 0.000792575155939161,
      inputAmount: 0.3,
      outputAmount: 1.2617
    };


    expect(tradeResults).toEqual(expectedSequenceResult);
  });
});
