import getTradeSequences, {TradeSequencesParams} from "./get-trade-sequences.function";
import {AlgorithmParams} from "../models/algorithm-params.model";
import {exampleTradingPairs, TradingPairs} from "../../shared/models/trading-pairs.model";

describe('getTradeSequences', () => {
	it('should return all trade sequences to a specified depth based on tradeMap', () => {
		const tradeMap = {
			sellMap: {
				'A': ['B', 'C', 'D'],
				'B': ['C'],
				'C': ['D'],
			},
			buyMap: {
				'B': ['A'],
				'C': ['A', 'B'],
				'D': ['A', 'C'],
			},
		};
		const algorithmParams: AlgorithmParams = {
      sourceCurrency: 'A',
      targetCurrency: 'A',
      maxDepth: 3
    };
		const tradeSequencesParams: TradeSequencesParams = {
		  currentCurrency: algorithmParams.sourceCurrency,
      tradingPairs: exampleTradingPairs,
      tradeMap,
      currentPairs: []
    };
		const expectedTradeSequences = [
			[ exampleTradingPairs['A-B'], exampleTradingPairs['B-C'], exampleTradingPairs['A-C'] ],
			[ exampleTradingPairs['A-C'], exampleTradingPairs['C-D'], exampleTradingPairs['A-D'] ],
			[ exampleTradingPairs['A-C'], exampleTradingPairs['B-C'], exampleTradingPairs['A-B'] ],
			[ exampleTradingPairs['A-D'], exampleTradingPairs['C-D'], exampleTradingPairs['A-C'] ],
		];
		const tradeSequences = getTradeSequences(algorithmParams, tradeSequencesParams);

    expect(tradeSequences).toEqual(expectedTradeSequences);
	});
});