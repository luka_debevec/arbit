import {AlgorithmParams} from "../models/algorithm-params.model";
import {TradingPairs} from "../../shared/models/trading-pairs.model";
import generatePairId from "../../shared/functions/generate-pair-id.function";

export interface TradeSequencesParams {
	tradingPairs: TradingPairs,
	tradeMap,
	currentCurrency: string,
	currentPairs
};

const getTradeSequences = (
	algorithmParams: AlgorithmParams,
	params: TradeSequencesParams,
	visited = {},
	results = []) => {
	const {
		tradingPairs,
		tradeMap,
		currentCurrency,
		currentPairs
	} = params;
	const {sellMap, buyMap} = tradeMap;
	const {targetCurrency, sourceCurrency, maxDepth} = algorithmParams;

	visited[currentCurrency] = true;

	if (sellMap[currentCurrency]) {
		let nextCurrencies = sellMap[currentCurrency];

		for (const nextCurrency of nextCurrencies) {
			const pairId = generatePairId(currentCurrency, nextCurrency);

			if (currentPairs.indexOf(tradingPairs[pairId]) > -1) {
				continue;
			}

			if (nextCurrency === targetCurrency) {
				currentPairs.push(tradingPairs[pairId]);
				results.push([...currentPairs]);
				currentPairs.splice(-1);
				continue;
			}

			if (visited[nextCurrency]) {
				continue;
			}

			currentPairs.push(tradingPairs[pairId]);
			if (currentPairs.length < maxDepth) {
				getTradeSequences(
					algorithmParams,
					{
						tradingPairs,
						tradeMap,
						currentCurrency: nextCurrency,
						currentPairs
					},
					visited,
					results);
			}
			currentPairs.splice(-1);
		}
	}

	if (buyMap[currentCurrency]) {
		let nextCurrencies = buyMap[currentCurrency];

		for (const nextCurrency of nextCurrencies) {
			const pairId = generatePairId(nextCurrency, currentCurrency);

			if (currentPairs.indexOf(tradingPairs[pairId]) > -1) {
				continue;
			}

			if (nextCurrency === targetCurrency) {
				currentPairs.push(tradingPairs[pairId]);
				results.push([...currentPairs]);
				currentPairs.splice(-1);
				continue;
			}

			if (visited[nextCurrency]) {
				continue;
			}

			currentPairs.push(tradingPairs[pairId]);
			if (currentPairs.length < maxDepth) {
				getTradeSequences(algorithmParams,
					{
						tradingPairs,
						tradeMap,
						currentCurrency: nextCurrency,
						currentPairs
					},
					visited,
					results);
			}
			currentPairs.splice(-1);
		}
	}

	delete visited[currentCurrency];

	return results;
};


export default getTradeSequences;