import {exampleTradingPairs} from '../../shared/models/trading-pairs.model';
import buildTradeMap from './build-trade-map';

describe('getTradeMap', () => {
  it('should return all the currencies with an array of target currencies', () => {
    const tradeMap = buildTradeMap(exampleTradingPairs);
    const expectedTradeMap = {
      sellMap: {
        'A': ['B', 'C', 'D'],
        'B': ['C'],
        'C': ['D'],
      },
      buyMap: {
        'B': ['A'],
        'C': ['A', 'B'],
        'D': ['A', 'C'],
      },
    };

    console.log(tradeMap);

    expect(tradeMap).toEqual(expectedTradeMap);
  });
});