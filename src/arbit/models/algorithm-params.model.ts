export interface AlgorithmParams {
  sourceCurrency: string,
  targetCurrency: string,
  maxDepth: number
}