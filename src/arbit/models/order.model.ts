import OrderType from "./order-type.enum";

export default interface Order {
	type: OrderType,
	isMarket?: boolean,
	amount: number,
	price: number,
	pair: string,
	baseCurrencyPrecision: number,
	quoteCurrencyPrecision: number,
}
