import Order from "./order.model";

export default interface TradeSequenceResult {
	orders: Order[],
	roundingLoss: number,
	inputAmount: number,
	outputAmount: number,
}