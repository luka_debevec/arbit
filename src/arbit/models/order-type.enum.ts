enum OrderType {
	Buy = 'BUY',
	Sell = 'SELL',
}

export default OrderType;