enum RoundingType {
	Down = 0,
	HalfUp = 1,
	HalfEven = 2,
	Up = 3,
}

export default RoundingType;