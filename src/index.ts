import App from "./app";
import Order from "./arbit/models/order.model";
import OrderType from "./arbit/models/order-type.enum";
import Big from 'big.js';

require('dotenv').config();

const exchangeName = process.env.EXCHANGE;
const exchangeSettings = process.env.EXCHANGE_SETTINGS;

async function start() {
	const app = new App(exchangeName, JSON.parse(exchangeSettings));
	await app.loadBalance();

	app.start();
}

start();


//
// async function buyByRate() {
// 	const app = new App(exchangeName, JSON.parse(exchangeSettings));
// 	await app.loadBalance();
// 	const tradePairs = await app
// 		.exchange
// 		.getTradePairs();
//
// 	const tickers = await app.exchange.getAllTickers();
// 	const filteredTickers = tickers
// 			.filter(({ symbol }) => {
// 				const [ baseCurrency, quoteCurrency ] = symbol.split('-');
//
// 				return quoteCurrency === 'BTC';
// 			});
//
// 	const sorted = filteredTickers
// 		.sort((a,b) => {
// 			if (+a.volValue === +b.volValue) {
// 				return 0;
// 			}
//
// 			return +a.volValue > +b.volValue ? -1 : 1;
// 		})
// 		.map((ticker) => ({
// 			...ticker,
// 			details: tradePairs[ticker.symbol]
// 		}));
//
// 	const orders: Order[] = [];
// 	const total = 0.00021;
//
// 	for (let i = 0; i < 60; i++) {
// 		await app.loadBalance();
// 		console.log(i);
//
// 		if (+app.balance.BTC < 0.0013) {
// 			break;
// 		}
//
// 		const ticker = sorted[i];
// 		const { details } = ticker;
// 		const { id } = details;
// 		const [ baseCurrency ] = id.split('-');
// 		const price = (new Big(ticker.buy))
// 			.times(1.005)
// 			.round(details.quoteCurrencyPrecision)
// 			.toFixed();
// 		const amount = (new Big(total)).div(price).round(details.baseCurrencyPrecision).toFixed();
// 		const currentBalance = app.balance[baseCurrency];
//
// 		if (currentBalance === undefined) {
// 			console.log(details);
// 		}
//
// 		if (currentBalance && amount * 0.90 > +currentBalance) {
// 			console.log(currentBalance);
// 			console.log(baseCurrency);
// 			const order: Order = {
// 				price: price,
// 				amount: amount,
// 				pair: id,
// 				type: OrderType.Buy,
// 				baseCurrencyPrecision: details.baseCurrencyPrecision,
// 				quoteCurrencyPrecision: details.quoteCurrencyPrecision
// 			};
// 			console.log(order);
//
// 			await app.exchange.fulfillOrder(order);
// 		} else {
// 			console.log('skip');
// 		}
// 	}
// }
//
//
// buyByRate();
