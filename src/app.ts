import getTradeSequences from "./arbit/functions/get-trade-sequences.function";
const fs = require('fs');
import Exchange from "./exchanges/models/exchange.model";
import KucoinExchangeService from "./exchanges/services/kucoin-exchange.service";
import {ArbitHelper, TradeSequence} from "./arbit/helpers/arbit.helper";
import {AlgorithmParams} from "./arbit/models/algorithm-params.model";
import Order from "./arbit/models/order.model";
import {ExchangeParameters} from "./shared/models/exchange-parameters.model";
import {Balance} from "./shared/models/balance.model";
import LoggerService from "./shared/services/logger.service";


export default class App {

	exchangeName: string = null;
	exchange: Exchange = null;
	exchangeParams: ExchangeParameters = null;
	sourceCurrency: string = null;
	logger: LoggerService = null;

	profit = 0;
	time = 0;
	iteration = 0;
	successfulIteration = 0;
	failedIterations = 0;
	maxIterations = 2000;
	maxSuccessIterations = 30;
	balance: Balance = {};
	threshold = 1;

	constructor(exchangeName, exchangeSettings) {
		this.logger = new LoggerService('logs');
		this.exchangeName = exchangeName;
		this.exchange = new KucoinExchangeService(exchangeSettings, this.logger);
		this.time = Date.now();
		this.exchangeParams = {
			fee: 0.001
		};
		this.sourceCurrency = 'BTC';
	}

	logError(error) {
		console.log('logError', error);
		this.logger.logError(JSON.stringify(error));
	}

	logBalance(balance) {
		console.log('logBalance');
		this.logger.logBalance(balance);
	}

	logWholeBalance(balance) {
		console.log('logWholeBalance');
		this.logger.logWholeBalance(JSON.stringify(balance));
	}

	async init() {
		try {
			// await this.convertToSourceCurrency();
			await this.loadBalance();
		} catch(err) {
			this.logError(err);
			await this.init();
		}
	}

	async start() {
		this
			.exchange
			.getTradePairs()
			.then(async (pairs) => {
				const algorithmParams: AlgorithmParams = {
					sourceCurrency: this.sourceCurrency,
					targetCurrency: this.sourceCurrency,
					maxDepth: 3,
				};
				const sourceCurrencyBalance = Math.min(+this.balance[this.sourceCurrency], 0.0005);
				const profitableSequence: TradeSequence = ArbitHelper
					.getProfitableTradeSequence(pairs, algorithmParams, this.exchangeParams, sourceCurrencyBalance, this.balance, this.threshold );


				if (profitableSequence) {
					this.logger.log('profitableSequence: ' + JSON.stringify(profitableSequence));
					this.profit += (profitableSequence.outputAmount-profitableSequence.inputAmount);
					this.logger.log('profit: ' + this.profit);
					return this
						.executeTradeSequence(profitableSequence)
						.then((result) => result ? 'success' : 'fail');
				}

				return 'skip';
			})
			.then(async (result) => {
				if (result === 'success') {
					this.successfulIteration++;
					this.logIterations();
					await this.init();
				} else if (result === 'fail') {
					this.failedIterations++;
					this.logIterations();
				}
				this.loop();
			})
			.catch(async (error) => {
				this.logError(error);
				await this.init();
				this.loop();
			})
	}

	logIterations() {
		const allIterations = this.successfulIteration + this.failedIterations;
		this.logger.log('iterations: ' + this.successfulIteration + '/' + allIterations);
	}

	async loadBalance() {
		const balance = await this.exchange.getBalance();

		this.logBalance(this.sourceCurrency + ': ' + balance[this.sourceCurrency]);
		this.logWholeBalance(balance);

		this.balance = balance;
	}

	async executeTradeSequence(tradeSequence: TradeSequence) {
		const orderResults = await this.placeOrders(tradeSequence);

		this.logger.log('executeTradeSequence: ' + JSON.stringify(orderResults));

		let success;

		if (orderResults.success) {
			return true;
		}

		await this.cancelAllOrders();

		// await this.convertToSourceCurrency();

		return false;
	}

	async placeOrders(tradeSequence: TradeSequence) {
		const { orders } = tradeSequence;
		const mappedOrders = orders.map((order) => ({
			...order,
			isMarket: true
		}));

		let orderRequests = mappedOrders.map(async (order) => await this.exchange.fulfillOrder(order));
		let results =  await Promise.all(orderRequests);
		this.logger.log('placeOrders: ' + JSON.stringify(results));

		for (let i = 0; i < results.length; i++) {
			const result = results[i];

			if (!result.success) {
				return { success: false };
			}
		}

		return { success: true };
	}

	async cancelAllOrders() {
		return this.exchange.cancelAllOrders();
	}

	async convertToSourceCurrency() {
		return;
		await this.loadBalance();
		const balance = this.balance;
		const currencies = Object.keys(balance);
		const eligibleCurrencies = [];
		const pairs = await this.exchange.getTradePairs();

		this.logger.log('convertToSourceCurrency');

		for (const currency of currencies) {
			if (currency === this.sourceCurrency) {
				continue;
			}

			const currencyBalance = +balance[currency] * 0.99;


			if (currencyBalance > 0) {
				const algorithmParams: AlgorithmParams = {
					sourceCurrency: currency,
					targetCurrency: this.sourceCurrency,
					maxDepth: 3,
				};
				const profitableSequence: TradeSequence = ArbitHelper
					.getProfitableTradeSequence(pairs, algorithmParams, this.exchangeParams, currencyBalance, balance,0 );

				if (profitableSequence) {
					this.logger.log('profitableSequence: ' + JSON.stringify(profitableSequence));
					eligibleCurrencies.push(profitableSequence);
				}

			}
		}

		if (!eligibleCurrencies.length) {
			return;
		}

		for (const tradeSequence of eligibleCurrencies) {
			const orderResults = await this.placeOrders(tradeSequence);
		}

		await this.convertToSourceCurrency();
	}

	writeToFile(profitableSequence: { inputAmount, outputAmount }) {
		const profit = profitableSequence.outputAmount - profitableSequence.inputAmount;

		this.profit = this.profit + profit;

		console.log(this.profit, (Date.now()-this.time)/ 1000);

	}

	loop() {
		this.iteration++;

		if (this.iteration%10 === 0) {
			this.logger.log('loop - ' + this.iteration);
		}

		if (this.successfulIteration >= this.maxSuccessIterations) {
			return;
		}
		if (this.iteration >= this.maxIterations) {
			return;
		}

			// get exchange order books
		setTimeout(() => {
			this.start();
		}, 1);
			// get
	}
}
