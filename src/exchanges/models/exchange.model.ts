import {TradingPairs} from "../../shared/models/trading-pairs.model";
import Order from "../../arbit/models/order.model";
import {OrderResult} from "./order-result.model";
import {Balance} from "../../shared/models/balance.model";

export default interface Exchange {

	getTradePairs() : Promise<TradingPairs>;

	fulfillOrder(order: Order): Promise<OrderResult>;

	cancelAllOrders(): Promise<any>;

	getAllTickers(): Promise<any>;

	getBalance(): Promise<Balance>;
}
