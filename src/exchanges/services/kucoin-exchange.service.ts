import LoggerService from "../../shared/services/logger.service";

const crypto = require('crypto');
import Big from 'big.js';
import Exchange from "../models/exchange.model";
import {TradingPairs} from "../../shared/models/trading-pairs.model";
import {OrderResult} from "../models/order-result.model";
import Order from "../../arbit/models/order.model";
import OrderType from "../../arbit/models/order-type.enum";
import {Balance} from "../../shared/models/balance.model";
import {getMinIncrement} from "../../arbit/functions/calculate-trade-result.function";

const fetch = require('node-fetch');

const sleep = (ms) => {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve();
		}, ms);
	});
};

interface PairOrderBook {
	sequence: string,
	bestAsk: string,
	size: string,
	price: string,
	bestBidSize: string,
	time: number,
	bestBid: string,
	bestAskSize: string
}
interface SymbolInfo {
	"symbol": string,
	"name": string,
	"baseCurrency": string,
	"quoteCurrency": string,
	"baseMinSize": string,
	"quoteMinSize": string,
	"baseMaxSize": string,
	"quoteMaxSize": string,
	"baseIncrement": string,
	"quoteIncrement": string,
	"priceIncrement": string,
	"feeCurrency": string,
	"enableTrading": true,
	"isMarginEnabled": true
}
interface KucoinOrder {
	side: 'buy' | 'sell',
	symbol: string,
	type: string,
	price: string,
	size: string,
	clientOid: string,
}
interface KucoinAccount {
	"id": string,
	"currency": string,
	"type": string,
	"balance": string,
	"available": string,
	"holds": string,
}

interface OrderStatus {
	isActive: boolean
}

export default class KucoinExchangeService implements Exchange {
	apiBasePath: string = 'https://api.kucoin.com';
	// apiBasePath: string = 'https://openapi-sandbox.kucoin.com/api/v1/accounts';
	tradePairSettings: any = null;
	symbols = null;
	exchangeSettings: any = null;
	loggerService: LoggerService = null;

	constructor(exchangeSettings, loggerService) {
		this.exchangeSettings = exchangeSettings;
		this.loggerService = loggerService;
	}

		/*
	public function signature($request_path = '', $body = '', $timestamp = false, $method = 'GET') {

		$body = is_array($body) ? json_encode($body) : $body; // Body must be in json format

		$timestamp = $timestamp ? $timestamp : time() * 1000;

		$what = $timestamp . $method . $request_path . $body;

		return base64_encode(hash_hmac("sha256", $what, $this->secret, true));
	}
		 */

	getHeaders(path, body = '', method = 'GET') {
		const {
			apiKey,
			apiSecret,
			apiPassphrase
		} = this.exchangeSettings;
		const formattedBody = body ? JSON.stringify(body) : '';
		const now = Date.now().toString();
		const message = now + method + path + formattedBody;
		const signature = crypto
			.createHmac('sha256', apiSecret)
			.update(message)
			.digest();

		const headers =  {
			'KC-API-SIGN': (new Buffer(signature)).toString('base64'),
			'KC-API-TIMESTAMP': now,
			'KC-API-KEY': apiKey,
			'KC-API-PASSPHRASE': apiPassphrase,
			'Content-Type': 'application/json'
		};


		return headers;
	}

	postRequest(path, body?: any) {
		const url = `${ this.apiBasePath }${ path }`
		const requestOptions: any = {
			method: 'POST',
			headers: this.getHeaders(path, body, 'POST'),
		};

		if (body) {
			requestOptions.body = JSON.stringify(body);
		}

		return fetch(url, requestOptions)
			.then((res) => {
				return res.json();
			});
	}

	getRequest(path, body?: any) {
		const url = `${ this.apiBasePath }${ path }`
		const requestOptions: any = {
			method: 'GET',
			headers: this.getHeaders(path, body, 'GET'),
		};

		return fetch(url, requestOptions)
			.then((res) => {
				return res.json();
			})
	}

	deleteRequest(path, body?: any) {
		const url = `${ this.apiBasePath }${ path }`
		const requestOptions: any = {
			method: 'DELETE',
			headers: this.getHeaders(path, body, 'DELETE'),
		};

		return fetch(url, requestOptions)
			.then((res) => res.json());
	}


	getSymbols(): Promise<SymbolInfo[]> {
		if (this.symbols) {
			return Promise.resolve(this.symbols);
		}

		const path = `/api/v1/symbols`;
		return this.getRequest(path)
			.then(({ data }) => {
				this.symbols = data;

				return data;
			});
	}

	/**
	 * Place order and wait for it to be fulfilled or cancel it and mark it as unsuccessful
	 * @param order
	 */
	async fulfillOrder(order: Order, count = 0): Promise<OrderResult> {
		let orderId = null;
		console.log('fulfilOrder', order);

		if (count > 5) {
			return { success: false };
		}


		for (let i = 0; i < 20; i++) {
			const orderResult = await this.placeOrder(order);

			if (orderResult.orderId) {
				orderId = orderResult.orderId;
				break;
			}
		}


		if (!orderId) {
			return { success: false };
		}

		let orderStatus = null;

		for (let i = 0; i < 10; i++) {
			orderStatus = await this.getOrderStatus(orderId);


			if (!orderStatus.isActive) {
				console.log('success');
				return count === 0 ? { success: true } : { success: false };
			}

			await sleep(300);
		}

		await this.cancelOrder(orderId);

		// Create new order if needed
		const newSize = +(new Big(order.amount).minus(orderStatus.dealSize).toString());

		if (newSize > 0) {
			const newOrder: Order = { ...order };
			if (newOrder.type === OrderType.Sell) {
				newOrder.price = +(new Big(newOrder.price).minus(getMinIncrement(order.quoteCurrencyPrecision)*2).round(order.quoteCurrencyPrecision).toString());
			} else {
				newOrder.price = +(new Big(newOrder.price).plus(getMinIncrement(order.quoteCurrencyPrecision)*2).round(order.quoteCurrencyPrecision).toString());
				newOrder.amount = +(new Big(newOrder.amount).minus(getMinIncrement(order.baseCurrencyPrecision)*2).round(order.baseCurrencyPrecision).toString());
			}

			return await this.fulfillOrder(newOrder, count+1);
		}


		//

		return { success: false };
	}

	placeOrder(order: Order): Promise<{ orderId: string }> {
		const path = `/api/v1/orders`;
		const kucoinOrder: KucoinOrder = {
			side: order.type === OrderType.Buy ? 'buy' : 'sell',
			symbol: order.pair,
			type: order.isMarket ? 'market' : 'limit',
			price: new Big(order.price).toFixed(),
			size: new Big(order.amount).toFixed(),
			clientOid: Date.now().toString()
		};

		this.loggerService.log('placeOrder - ' + JSON.stringify(kucoinOrder));

		return this
			.postRequest(path, kucoinOrder)
			.then((result) => {
				console.log(result);

				return result.data ? result.data : {};
			});
	}

	getOrderStatus(orderId: string): OrderStatus {
		const path = `/api/v1/orders/${ orderId }`;
		this.loggerService.log('getOrderStatus - ' + JSON.stringify(orderId));

		return this
			.getRequest(path)
			.then(({ data }) => data || null);
	}

	async getAccounts(): Promise<KucoinAccount[]> {
		const path = `/api/v1/accounts`;

		return this.getRequest(path)
			.then((result) => result && result.data ? result.data : []);
	}

	async getBalance(): Promise<Balance> {
		const accounts: KucoinAccount[] = await this.getAccounts();
		const balance: Balance = {};

		for (const account of accounts) {
			const {
				currency,
				available
			} = account;

			if (balance[currency]) {
				balance[currency] = (new Big(balance[currency])).add(available).toFixed();
			} else {
				balance[currency] = available;
			}
		}

		return balance;
	}


	async getAllTickers(): Promise<KucoinAccount[]> {
		const path = `/api/v1/market/allTickers`;

		return this.getRequest(path)
			.then((result) => result && result.data.ticker ? result.data.ticker : []);
	}

	async cancelAllOrders(): Promise<any> {
		const path = `/api/v1/orders`;
		this.loggerService.log('cancelAllOrders');

		return this.deleteRequest(path);
	}

	async cancelOrder(orderId): Promise<any> {
		const path = `/api/v1/orders/${ orderId }`;
		this.loggerService.log('cancelOrder: ' + orderId);

		return this.deleteRequest(path);
	}


	getTradePairSettings(): Promise<any> {


		return Promise.resolve({});
	}

	getPrecision(minIncrement: string) {
		const [, decimal] = minIncrement.split('.');

		return decimal ? decimal.length : 0;
	}

	getOrderBook() {
		return this
			.getSymbols()
			.then((tradingSymbols) => {
				return Promise.all(tradingSymbols.map(({ symbol }) => this.getPairOrderBook(symbol)))
					.then((pairOrderBooks) => {
						const tradingPairs: TradingPairs = {};

						for (let i = 0; i < tradingSymbols.length; i++) {
							const tradingSymbol = tradingSymbols[i];
							const pairOrderBook = pairOrderBooks[i];

							const tradePair = {
								id: tradingSymbol.symbol,
								baseCurrency: tradingSymbol.baseCurrency,
								quoteCurrency: tradingSymbol.quoteCurrency,
								minAmount: +tradingSymbol.baseMinSize,
								minTotal: +tradingSymbol.quoteMinSize,
								ask: {price: +pairOrderBook.bestAsk, amount: +pairOrderBook.bestAskSize},
								bid: {price: +pairOrderBook.bestBid, amount: +pairOrderBook.bestBidSize},
								baseCurrencyPrecision: this.getPrecision(tradingSymbol.baseIncrement),
								quoteCurrencyPrecision: this.getPrecision(tradingSymbol.priceIncrement),
							};

							if (tradePair.ask.price && tradePair.bid.price) {
								tradingPairs[tradingSymbol.symbol] = tradePair;
							}
						}

						return tradingPairs;
					})
			})
			.then((data) => {
				return data;
			});
	}

	getPairOrderBook(pairId): Promise<PairOrderBook> {
		const path = `/api/v1/market/orderbook/level1?symbol=${ pairId }`;

		return this
			.getRequest(path)
			.then(({ data }) => {
				return data;
			});
	}

	getTradePairs(): Promise<TradingPairs> {
		return this
			.getOrderBook();
	}

}
